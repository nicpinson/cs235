#include "Critter.hpp"

#include <iostream>

#include "utilities/StringUtil.hpp"
using namespace std;

Critter::Critter()
{
    m_alive = true;
    m_timeAlive = 0;
    m_goalX = -1;
    m_goalY = -1;
    m_eaten = 0;
}

void Critter::Draw()
{
    if ( m_alive )
    {
        cout << m_symbol;
    }
}

void Critter::Update()
{
    if ( !m_alive )
    {
        // Doesn't do anything
        if (m_name == "Cash Cow")
        {
            m_log = "[" + m_name + " has been caught.]";
        }
        else
        {
            m_log = "[" + m_name + " is bird food. Time Alive: " + StringUtil::ToString(m_timeAlive) + "]";
        }
        
        return;
    }

    m_timeAlive++;

    if ( m_goalX != -1 && m_goalX < m_x )
    {
        m_x--;
        m_log = "[" + m_name + " moved left!]";
    }
    else if ( m_goalX != -1 && m_goalX > m_x )
    {
        m_x++;
        m_log = "[" + m_name + " moved right!]";
    }

    else if ( m_goalY != -1 && m_goalY < m_y )
    {
        m_y--;
        m_log = "[" + m_name + " moved up!]";
    }
    else if ( m_goalY != -1 && m_goalY > m_y )
    {
        m_y++;
        m_log = "[" + m_name + " moved down!]";
    }
    else
    {
        m_log = "[" + m_name + " rested.]";
    }

    if ( m_x == m_goalX && m_y == m_goalY )
    {
        // Reset goal
        m_goalX = m_goalY = -1;
    }
}

void Critter::SetGoal( int x, int y )
{
    m_goalX = x;
    m_goalY = y;
}

bool Critter::HasGoal()
{
    return ( m_goalX != -1 && m_goalY != -1 );
}

void Critter::CaughtWorm()
{
    m_eaten++;
    m_log = "[" + m_name + " caught a worm!]";
    m_goalX = -1;
    m_goalY = -1;
}

void Critter::OutputLog()
{
    cout << m_log << endl;
}

void Critter::CaughtCow()
{
    m_log = "[" + m_name + " feels a little richer!]";
    m_goalX = -1;
    m_goalY = -1;
}