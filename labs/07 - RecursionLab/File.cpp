#include "File.hpp"

File::File() {}

File::File( string name, string contents )
    : FileSystemThing( name, "File" )
{
    Setup( contents );
}

void File::Setup( string contents )
{
    m_contents = contents;
    m_bytes = sizeof( m_contents );
}

void File::Display()
{
    FileSystemThing::Display();
}

void File::Details()
{
    FileSystemThing::Details();
    cout << "Size:              " << m_bytes << " bytes" << endl;
    cout << "Contents:          " << m_contents << endl;
    cout << endl;
}
