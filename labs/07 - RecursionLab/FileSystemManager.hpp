#ifndef _FILE_SYSTEM_MANAGER
#define _FILE_SYSTEM_MANAGER

#include "File.hpp"
#include "Folder.hpp"

#include <vector>
using namespace std;

class FileSystemManager
{
    public:
    FileSystemManager();
    ~FileSystemManager();

    void Display();
    File*   FindFile( string name );
    Folder* FindFolder( string name );

    private:
    void Recursive_DisplayFile( File* ptrFile, int level );
    void Recursive_DisplayFolder( Folder* ptrFolder, int level );

    File* Recursive_FindFile( Folder* ptrFolder, string name );
    Folder* Recursive_FindFolder( Folder* ptrFolder, string name );

    void Indent( int level );
    vector<File*>    m_allFiles;
    vector<Folder*>  m_allFolders;
};

#endif
