#include "Shapes.hpp"


/* RECTANGLE ***********************************************************/
/***********************************************************************/

void Rectangle::Setup( float width, float height, string unit )
{
    Shape::Setup(unit);
    m_width = width;
    m_height = height;
}

void Rectangle::DisplayArea()
{
    float tempArea = Rectangle::CalculateArea();
    cout << tempArea << " " << m_unit << "^2" << endl;
}

float Rectangle::CalculateArea()
{
    float area = m_width * m_height;
    return area;
}

/* CIRCLE **************************************************************/
/***********************************************************************/

void Circle::Setup( float radius, string unit )
{
    Shape::Setup(unit);
    m_radius = radius;
}

void Circle::DisplayArea()
{
    float tempArea = Circle::CalculateArea();
    cout << tempArea << " " << m_unit << "^2" << endl;
}

float Circle::CalculateArea()
{
    float pi = 3.14;
    float area = pi * m_radius * m_radius;
    return area;
}

/* SHAPE (parent class) FUNCTIONS **************************************/
/***********************************************************************/

void Shape::Setup( string unit )
{
    m_unit = unit;
}

void Shape::DisplayArea()
{
    cout << " " << m_unit << "^2";
}

// See Shapes.hpp, using a "pure virtual function" to implement Abstract Classes

//float Shape::CalculateArea()
//{
//    return 0;
//}
