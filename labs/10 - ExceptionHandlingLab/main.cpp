#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdexcept>
using namespace std;

float Division( int num, int denom )
{
    // TODO: Check for division by 0
    if (denom == 0) {
        throw runtime_error("Cannot divide by zero!");
    }
    return float(num) / float(denom);
}

void Test_Division( int min, int max )
{
    ofstream output( "result.txt" );
    for ( int n = min; n <= max; n++ )
    {
        for ( int d = min; d <= max; d++ )
        {
            output << left <<
                setw( 3 ) << n <<
                setw( 3 ) << "/" <<
                setw( 3 ) << d << " = ";

            float result = 0;

            // TODO: Wrap this in a try/catch
            try
            {
                result = Division(n, d);
            }
            catch(runtime_error ex)
            {
                output << ex.what() << endl;
                continue;
            }
            output << setw( 20 ) << result << endl;
        }
    }
}

int main()
{
    Test_Division(-2, 2 );

    return 0;
}
