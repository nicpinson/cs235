#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Menu.hpp"

int RollDie();
void Program1();
void Program2();

int main()
{
    srand( time( NULL ) );

    bool done = false;
    while ( !done )
    {
        Menu::Header( "Dynamic Arrays Lab" );
        int choice = Menu::ShowIntMenuWithPrompt(
        {
            "Program 1: New and Delete",
            "Program 2: Resize Array",
            "Quit"
        } );

        switch( choice )
        {
            case 1: Program1(); break;
            case 2: Program2(); break;
            default: done = true;
        }
    }

    return 0;
}

void Program1()
{
    Menu::Header( "Program 1: New and Delete" );

    int totalDieRolls;  // # of times to roll
    int * dieRolls;     // array of roll results
    float rollSum = 0;  // sum of roll results
    float averageValue; // calculated average

    cout << "How many die rolls? ";
    cin >> totalDieRolls;

    // TODO: Allocate space for the new array of size totalDieRolls
    // via the dieRolls pointer.
    dieRolls = new int[totalDieRolls];

    cout << "Rolling die... ";
    for ( int i = 0; i < totalDieRolls; i++ )
    {
        // Roll die and store result
        dieRolls[i] = RollDie();
        cout << dieRolls[i] << " ";
        // Add on to the sum
        rollSum += dieRolls[i];
    }

    // Calculate the average
    averageValue = rollSum / totalDieRolls;
    cout << endl << "The average value is: " << averageValue << endl;

    // TODO: Free the allocated space.
    delete[] dieRolls;

    cout << endl << endl;
}

void Program2()
{
    Menu::Header( "Program 2: Resize Array" );

    int size1, size2;

    cout << "Enter the size of the first array: ";
    cin >> size1;

    cout << "Enter the size of the second array (bigger than the first): ";
    cin >> size2;

    // TODO: Follow instructions from document
    int* arr1 = new int[size1];

    cout << "Array contents: ";
    for (int i = 0; i < size1; i++)
    {
        arr1[i] = RollDie();
        cout << arr1[i] << " ";
    }
    cout << endl;

    int* arr2 = new int[size2];

    for (int i = 0; i < size1; i++)
    {
        arr2[i] = arr1[i];
    }

    // Free old memory
    delete[] arr1;

    // Update where arr1 is pointing to:
    arr1 = arr2;

    // Set arr2 to nullptr since we around using it anymore
    arr2 = nullptr;

    // Fill the remainder of the array:
    for (int i = size1; i < size2; i++)
    {
        arr1[i] = RollDie();
    }

    // Display the entire contents of the array again
    cout << "Array contents: ";
    for (int i = 0; i < size2; i++)
    {
        cout << arr1[i] << " ";
    }
    cout << endl << endl;

    delete[] arr1;
}

int RollDie()
{
    return rand() % 6 + 1;
}
