#include "Image.hpp"

#include <fstream>
#include <iostream>
#include <exception>
#include <vector>
using namespace std;

PpmImage::PpmImage()
{
    m_pixels = new Pixel*[IMAGE_WIDTH];

    for ( int i = 0; i < IMAGE_WIDTH; i++ )
    {
        m_pixels[i] = new Pixel[IMAGE_HEIGHT];
    }
}

PpmImage::~PpmImage()
{
    for ( int i = 0; i < IMAGE_WIDTH; i++ )
    {
        delete [] m_pixels[i];
    }

    delete [] m_pixels;
}

void PpmImage::LoadImage( const string& filename )
{
    cout << "Loading image from \"" << filename << "\"...";

    //throw runtime_error( "Method not implemented!" );

    ifstream input(filename);

    if (input.fail())
    {
        throw runtime_error("File open failed!");
    }

    string strBuffer;
    int intBuffer;

    getline(input, strBuffer); // Skip "P3"
    getline(input, strBuffer); // Skip comment

    input >> intBuffer; // Skip width
    input >> intBuffer; // Skip height

    input >> m_colorDepth;

    int x = 0;
    int y = 0;

    while (input >> m_pixels[x][y].r
                 >> m_pixels[x][y].g
                 >> m_pixels[x][y].b)
    {
        x++;
        if (x == IMAGE_WIDTH)
        {
            y++;
            x = 0;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::SaveImage( const string& filename )
{
    cout << "Saving image to \"" << filename << "\"...";

    //throw runtime_error( "Method not implemented!" );
    ofstream output(filename);

    if (output.fail())
    {
        throw runtime_error( "Failed to open file!" );
    }

    output << "P3" << endl;
    output << "# Comment" << endl;
    output << IMAGE_WIDTH << " " << IMAGE_HEIGHT << endl;
    output << m_colorDepth << endl;

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_HEIGHT; x++)
        {
            output
                << m_pixels[x][y].r << endl
                << m_pixels[x][y].g << endl
                << m_pixels[x][y].b << endl;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter1()
{
    cout << "Applying filter 1...";

    //throw runtime_error( "Method not implemented!" );
    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            int y2 = IMAGE_HEIGHT - 1 - y;
            int x2 = IMAGE_WIDTH - 1 - x;

            m_pixels[x][y].r = m_pixels[x2][y2].r;
            m_pixels[x][y].g = m_pixels[x2][y2].g;
            m_pixels[x][y].b = m_pixels[x2][y2].b;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter2()
{
    cout << "Applying filter 2...";

    //throw runtime_error( "Method not implemented!" );
    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            int r = m_pixels[x][y].r;
            int g = m_pixels[x][y].g;
            int b = m_pixels[x][y].b;

            m_pixels[x][y].r = g;
            m_pixels[x][y].g = b;
            m_pixels[x][y].b = r;
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter3()
{
    cout << "Applying filter 3...";

    //throw runtime_error( "Method not implemented!" );
    int brightenAmount = 50;
    int rbgMax = 255;

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        for (int x = 0; x < IMAGE_WIDTH; x++)
        {       
            int r = m_pixels[x][y].r;
            int g = m_pixels[x][y].g;
            int b = m_pixels[x][y].b;

            if (r <= (rbgMax - brightenAmount))
            {
                m_pixels[x][y].r = r + brightenAmount;
            }
            else
            {
                m_pixels[x][y].r = 255;
            }
            if (g <= (rbgMax - brightenAmount))
            {
                m_pixels[x][y].g = g + brightenAmount;
            }
            else
            {
                m_pixels[x][y].g = 255;
            }
            if (b <= (rbgMax - brightenAmount))
            {
                m_pixels[x][y].b = b + brightenAmount;
            }
            else
            {
                m_pixels[x][y].b = 255;
            }
        }
    }

    cout << "SUCCESS" << endl;
}

void PpmImage::ApplyFilter4()
{
    cout << "Applying filter 4...";

    //throw runtime_error( "Method not implemented!" );

    for (int y = 0; y < IMAGE_HEIGHT; y++)
    {
        const int numOfColors = 3;
        int inverseArray[IMAGE_WIDTH * numOfColors];
        int countDown = IMAGE_WIDTH * numOfColors - 1;
        int countUp = 0;

        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            inverseArray[countUp] = m_pixels[x][y].r;
            countUp++;
            inverseArray[countUp] = m_pixels[x][y].g;
            countUp++;
            inverseArray[countUp] = m_pixels[x][y].b;
            countUp++;
        }

        for (int x = 0; x < IMAGE_WIDTH; x++)
        {
            m_pixels[x][y].r = inverseArray[countDown];
            countDown--;
            m_pixels[x][y].g = inverseArray[countDown];
            countDown--;
            m_pixels[x][y].b = inverseArray[countDown];
            countDown--;
        }
    }

    cout << "SUCCESS" << endl;
}