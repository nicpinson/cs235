#include "Question.hpp"

#include "Exceptions/NotImplementedException.hpp"

#include <iostream>
using namespace std;

//! Default constructor - Initialize m_question and m_answer to empty strings.
Question::Question()
{
    m_question = "";
}

//! Parameterized constructor - Call the Setup function to initialize the question text.
Question::Question( string question )
{
    Setup( question );
}

//! Set up m_question and m_answer using the parameters.
void Question::Setup( string question )
{
    m_question = question;
}

//! Display the question text.
void Question::Display()
{
    cout << "-----------------------------" << endl;
    cout << m_question << endl << endl;
}

/****************************************************************************************/
/* MultipleChoiceQuestion                                                               */
/****************************************************************************************/

// TODO: Implement functions
void MultipleChoiceQuestion::Setup(string question)
{
    m_question = question;
}

void MultipleChoiceQuestion::Display()
{
    Question::Display();

    cout << "Possible answers:" << endl;
    int i = 0;
    while (i < 4)
    {
        cout << (i + 1) << ". " << m_options[i] << endl;
        i++;
    }
}

bool MultipleChoiceQuestion::RunQuestion()
{
    MultipleChoiceQuestion::Display();

    cout << "Your answer: ";
    int userChoice;
    cin >> userChoice;
    
    if ((userChoice - 1) == m_answer)
    {
        cout << endl << "Correct!" << endl;
        return true;
    }
    else
    {
        cout << endl << "WRONG! The answer was " << (m_answer + 1) << endl;
        return false;
    }
}

void MultipleChoiceQuestion::LoadQuestion( istream& in )
{
    string buffer;
    while ((getline(in, buffer)) && (buffer != "QUESTION_END"))
    {
        if (buffer == "QUESTION")
        {
            getline(in, m_question);
        }
        if (buffer == "OPTION0")
        {
            getline(in, m_options[0]);
        }
        if (buffer == "OPTION1")
        {
            getline(in, m_options[1]);
        }
        if (buffer == "OPTION2")
        {
            getline(in, m_options[2]);
        }
        if (buffer == "OPTION3")
        {
            getline(in, m_options[3]);
        }
        if (buffer == "ANSWER")
        {
            in >> m_answer;
        }
    }
}


/****************************************************************************************/
/* TrueFalseQuestion                                                                    */
/****************************************************************************************/

// TODO: Implement functions
void TrueFalseQuestion::Setup(string question)
{
    m_question = question;
}

void TrueFalseQuestion::Display()
{
    Question::Display();
    cout << "1. True, or 2. False?" << endl;
}

bool TrueFalseQuestion::RunQuestion()
{
    TrueFalseQuestion::Display();
    cout << "Your answer: ";
    int userChoice;
    cin >> userChoice;

    if (userChoice == m_answer)
    {
        cout << endl << "Correct!" << endl;
        return true;
    }
    else
    {
        cout << endl << "WRONG! The answer was " << m_answer << endl;
        return false;
    }
}

void TrueFalseQuestion::LoadQuestion(istream& in)
{
    string buffer;
    while ((getline(in, buffer)) && (buffer != "QUESTION_END"))
    {
        if (buffer == "QUESTION")
        {
            getline(in, m_question);
        }
        if (buffer == "ANSWER")
        {
            in >> m_answer;
        }
    }
}


/****************************************************************************************/
/* FillInTheBlankQuestion                                                               */
/****************************************************************************************/

// TODO: Implement functions
void FillInTheBlankQuestion::Setup(string question)
{
    m_question = question;
}

void FillInTheBlankQuestion::Display()
{
    Question::Display();
}

bool FillInTheBlankQuestion::RunQuestion()
{
    FillInTheBlankQuestion::Display();
    cout << "Fill in your answer: ";
    string userChoice;
    cin >> userChoice;

    if (userChoice == m_answer)
    {
        cout << endl << "Correct!" << endl;
        return true;
    }
    else
    {
        cout << endl << "WRONG! The answer was " << m_answer << endl;
        return false;
    }
}

void FillInTheBlankQuestion::LoadQuestion(istream& in)
{
    string buffer;
    while ((getline(in, buffer)) && (buffer != "QUESTION_END"))
    {
        if (buffer == "QUESTION")
        {
            getline(in, m_question);
        }
        if (buffer == "ANSWER")
        {
            getline(in, m_answer);
        }
    }
}
