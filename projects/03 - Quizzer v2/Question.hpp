#ifndef _QUESTION
#define _QUESTION

#include <string>
#include <istream>
using namespace std;

class Question
{
    public:
    Question();
    Question( string question );
    void Setup( string question );
    virtual void Display() = 0;
    virtual bool RunQuestion() = 0;
    virtual void LoadQuestion( istream& in ) = 0;

    protected:
    string m_question;

};

// TODO: Declare child classes
class MultipleChoiceQuestion : public Question
{
    public:
        void Setup(string question);
        void Display();
        bool RunQuestion();
        void LoadQuestion( istream& in );
    private:
        int m_answer;
        string m_options[4];
};

class TrueFalseQuestion : public Question
{
    public:
        void Setup(string question);
        void Display();
        bool RunQuestion();
        void LoadQuestion(istream& in);
    private:
        int m_answer;
};

class FillInTheBlankQuestion : public Question
{
    public:
        void Setup(string question);
        void Display();
        bool RunQuestion();
        void LoadQuestion(istream& in);
    private:
        string m_answer;
};

#endif
