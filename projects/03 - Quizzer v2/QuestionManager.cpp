#include "QuestionManager.hpp"

#include "Exceptions/NotImplementedException.hpp"

#include <iostream>
#include <fstream>
using namespace std;

vector<Question*> QuestionManager::m_questions;
int QuestionManager::m_currentIndex;

//! Load the quiz questions from a text file.
void QuestionManager::LoadQuestions( string filename )
{
    ifstream input( filename );
    string buffer;
    string questionType;

    while ( input >> buffer )
    {
        if ( buffer == "TYPE" )
        {
            input >> questionType;
            Question* ptrQuestion = nullptr;

            if ( questionType == "fitb" )
            {
                // TODO: Instantiate a FillInTheBlankQuestion here.
                ptrQuestion = new FillInTheBlankQuestion;
            }
            else if ( questionType == "mc" )
            {
                // TODO: Instantiate a MultipleChoiceQuestion here.
                ptrQuestion = new MultipleChoiceQuestion;
            }
            else if ( questionType == "tf" )
            {
                // TODO: Instantiate a TrueFalseQuestion here.
                ptrQuestion = new TrueFalseQuestion;
            }

            ptrQuestion->LoadQuestion( input );
            m_questions.push_back( ptrQuestion );
        }
    }

    cout << endl << m_questions.size() << " questions loaded" << endl;
}

//! Load the quiz questions from a text file.
void QuestionManager::CleanupQuestions()
{
    for ( auto& questionPtr : m_questions )
    {
        if ( questionPtr != nullptr )
        {
            delete questionPtr;
            questionPtr = nullptr;
        }
    }

    m_questions.clear();
    m_currentIndex = 0;
}

//! Return the current question and increment the index counter.
Question* QuestionManager::GetNextQuestion()
{
    return m_questions[ m_currentIndex++ ];
}

//! Returns whether we're out of questions or not.
bool QuestionManager::IsEmpty()
{
    return m_currentIndex == m_questions.size();
}
